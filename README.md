# Beschreibung

# Token.java
	Im zuge meiner Arbeit bei der Allianz, habe ich einen Token-Request-Tool auf eine Webseite macht, falls noch kein Token vorhanden ist oder die gültigkeit des Tokens abgelaufen ist.
	Das Tool wurde zum Testen geschrieben.
# MedikationsplanActivity.java
	Bei einem Coding-Competition im Zuge meines Studiums, wurde eine Android-App (in Java mit Android Studio) entwickelt.
# map.js
	Im Studium (Webprogrammierung) wurde eine Webseite entwickelt, bei denen Standortdaten eines Clubs mit einer Bewertung übermittelt wird.
	Diese hochgeladene Daten wurden daraufhin auf der Webseite mit Hilfe des Google-APIs auf einer Stadtkarte angezeigt.
# diagram.hbs
	Im Studium (Softwarepraktikum) wurde eine eigensändige App entwickelt, die gewisse Daten "tracken" konnte. Dieser wurde über einen Telegram Bot übermittelt.
	Die übermittelten Daten konnten dann über eine Webseite (mit Handlebars Template Engine) als Diagramm (vis.js) angezeigt und gefiltert werden.
# copy-config-clustertls.sh
	Bei der ARbeit bei der Allianz, wurde ein Cluster von ArtemisMQ Systeme aufgebaut. Die Konfigurationsdateien mussten pro Server speziell angepasst werden.
	Hierzu wurde eine XML-Template erstellt und mit Hilfe des Bash-Scripts auf die einzelnen Server verteilt.