/**
 * Created by micha on 05.07.2017.
 */
var position = {lat: 50.941357, lng: 6.958307};
var map;
var myCoords;
var myMarker;
var x = document.getElementById("coords");
var y = document.getElementById("address");

// Function is called when button is pressed
function getPosition() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPositionSuccess, showPositionError);
    } else {
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

// Callback Function after getting current Position successfull
function showPositionSuccess(position) {
    myCoords = {lat: position.coords.latitude, lng: position.coords.longitude};
    $.ajax({
        type: 'GET',
        url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + myCoords.lat + ',' + myCoords.lng + '&key=' + apiKey,
        dataType: "json",
        success: function (result) {
            y.innerHTML = "Du bist hier: " + result['results'][0].formatted_address;
            x.innerHTML = "Latitude: " + myCoords.lat +
                "<br>Longitude: " + myCoords.lng;

            // Set Marker after Button is pressed
            if (myMarker == null) {
                myMarker = new google.maps.Marker({
                    title: 'ich bin hier',
                    position: myCoords,
                    map: map,
                });
            } else {
                myMarker.setPosition(myCoords);
            }
            map.setCenter(myCoords);
            map.setZoom(14);
        }
    });
}

// Callback Function after getting current Position not successfull
function showPositionError(position) {
    x.innerHTML = "Geolocation is not supported by this browser.";
}

// Callback Function after Google Map API is loaded
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: position
    });
    var imgIcon = 'assets/img/beer.png';
    $.ajax({
        dataType: 'json',
        crossDomain: 'true',
        type: 'GET',
        url: '/location?format=json',
        success: function ($result) {
            $.each($result, function ($i, location) {
                var locPosition = {lat: location.geoLatitude, lng: location.geoLongitude};
                var locMarker = new google.maps.Marker({
                    title: location.name,
                    position: locPosition,
                    map: map,
                    icon: imgIcon
                });
                locMarker.addListener('click', function () {
                    window.open('location/' + location.idLocation, '_blank');
                });
            });
        }
    });
}