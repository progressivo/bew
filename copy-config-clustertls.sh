#!/usr/bin/env bash
is=(0 1 2)
BROKER_HOSTS=(blue.dev.a blue.dev.b blue.dev.c)
BROKER_IP=(10.xx.xxx.18 10.xx.xxx.123 10.xx.xxx.173)
BROKER_HOSTNAME=(message-broker.blue.dev.a1 message-broker.blue.dev.b1 message-broker.blue.dev.c1)
BROKER_LOCALPORT=(xxxx5 xxxx6 xxxx7)
BROKER_NODENAME=(node_a_connector node_b_connector node_c_connector)

wait; cp etc/broker_clustertls.xml etc/broker_clustertls_tmp.xml &

for i in "${is[@]}"
do
    wait; sed -i -e "s/BROKER_${i}_IP/${BROKER_IP[i]}/g" etc/broker_clustertls_tmp.xml &
    wait; sed -i -e "s/BROKER_${i}_LOCALPORT/${BROKER_LOCALPORT[i]}/g" etc/broker_clustertls_tmp.xml &
done


for i in "${is[@]}"
do
    cp etc/broker_clustertls_tmp.xml etc/broker_clustertls_${i}.xml &
    wait; sed -i -e "s/BROKER_HOSTNAME/${BROKER_HOSTNAME[i]}/g" etc/broker_clustertls_${i}.xml &
    wait; sed -i -e "s/BROKER_IP/${BROKER_IP[i]}/g" etc/broker_clustertls_${i}.xml &
    wait; sed -i -e "s/default-discovery-group/${BROKER_HOSTS[i]}/g" etc/broker_clustertls_${i}.xml &
    wait; sed -i -e "s/BROKER_LOCALPORT/${BROKER_LOCALPORT[i]}/g" etc/broker_clustertls_${i}.xml &
    wait; sed -i "/<connector-ref>${BROKER_NODENAME[i]}<\\/connector-ref>/d" etc/broker_clustertls_${i}.xml &
    wait; sed -i -e "s/BROKER_NODENAME/${BROKER_NODENAME[i]}/g" etc/broker_clustertls_${i}.xml &

    echo "Copy Settings to ${BROKER_HOSTS[i]}"
    wait; rsync etc/broker_clustertls_${i}.xml ${BROKER_HOSTS[i]}:/home/ec2-user/broker.xml &
    wait; rsync etc/broker-keystore.p12 etc/broker-truststore.jks ${BROKER_HOSTS[i]}:/home/ec2-user &
    wait; ssh -t ${BROKER_HOSTS[i]} <<'EOSSH'
        sudo su;
        if [ -e broker.xml ]; then
            cp -u broker.xml /opt/amq/etc;
            mv -u broker.xml /opt/artemis/etc;
        fi;
        if [ -e broker-keystore.p12 ]; then
            cp -u broker-keystore.p12 /opt/amq/etc;
            mv -u broker-keystore.p12 /opt/artemis/etc;
        fi;
        if [ -e broker-truststore.jks ]; then
            cp -u broker-truststore.jks /opt/amq/etc;
            mv -u broker-truststore.jks /opt/artemis/etc;
        fi;
EOSSH
    rm etc/broker_clustertls_${i}.xml
done
rm etc/broker_clustertls_tmp.xml