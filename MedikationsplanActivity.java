package de.michael_pham.medikationsplanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import de.michael_pham.medikationsplanner.bean.Medikationsplan;
import de.michael_pham.medikationsplanner.service.MedikationsPlanHandler;
import de.michael_pham.medikationsplanner.service.MedikationsPlanService;

/**
 * Created by mpham on 16.11.2016.
 */

public class MedikationsplanActivity extends Activity
        implements MedikationsplanFragment.OnListFragmentInteractionListener {
    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    final Messenger mMessenger = new Messenger(new ReceivedNotificationHandler());
    /*
     * --------------------------------------------------------------------------------------------
     * ------------------------------- LIST UPDATE HANDLER ----------------------------------------
     */
    private List<Medikationsplan> mpl = new ArrayList<>();
    private Fragment mpFragment = new MedikationsplanFragment();
    /*
     * --------------------------------------------------------------------------------------------
     * ---------------------------------- SERVICE KOMPONENTEN -------------------------------------
     */
    private MedikationsPlanService mph;
    private MedikationsPlanService.MedikationsplanBinder mBinder;
    private boolean mBound = false;
    private boolean mMBound = false;
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mBinder = (MedikationsPlanService.MedikationsplanBinder) service;
            Log.d("MedikationsPlan", "MedikationsplanActivity: ismBinderAlive(): " + mBinder.isBinderAlive());
            mph = mBinder.getService();
            mBound = true;
            // Register this komponent in the Service
        }

        public void onServiceDisconnected(ComponentName className) {
            Log.e("MedikationsPlan", "onServiceDisconnected");
            mBound = false;
        }
    };
    /**
     * Service Connection für den MESSENGER, der Benachrichtigungen über Updates erhält
     */
    private Messenger mService;
    private ServiceConnection messengerConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            try {
                mService = new Messenger(service);
                Message msg = Message.obtain(null, MedikationsPlanHandler.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger; // set 'messenger' as response messenger
                mService.send(msg);
                mMBound = true;
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mMBound = false;
        }
    };

    /**
     * Wenn ZXING QR Scanner nicht installiert wird, wird ein ALERTDIALOG geöffnet, bei dem man
     * die APP herunterladen kann.
     *
     * @param act
     * @param title
     * @param message
     * @param buttonYes
     * @param buttonNo
     * @return
     */
    private static AlertDialog showDialog(final Activity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }

    /*
     * --------------------------------------------------------------------------------------------
     * --------------------------------- ACTIVITY KOMPONENTEN -------------------------------------
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Bind Service Connection
        ComponentName componentService = startService(new Intent(this, MedikationsPlanService.class));
        Intent intent = new Intent(getApplicationContext(), MedikationsPlanService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        ComponentName componentHandler = startService(new Intent(this, MedikationsPlanHandler.class));
        Intent intentMessenger = new Intent(getApplicationContext(), MedikationsPlanHandler.class);
        bindService(intentMessenger, messengerConnection, Context.BIND_AUTO_CREATE);

        setContentView(R.layout.activity_medikationsplan);

        // Create Fragment
        mpFragment = new MedikationsplanFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentcontainer, mpFragment);
        transaction.commit();

        // Add Floating Button onClickListeners
        FloatingActionButton fabAdd = (FloatingActionButton) findViewById(R.id.mp_fabAdd);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openZxingQRScanner();
            }
        });
        fabAdd.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                openXmlInsertDialog(v);
                return false;
            }
        });

        refreshView();
    }

    /*
     * --------------------------------------------------------------------------------------------
     * ------------------------------ FRAGMENT CONTROL --------------------------------------------
     */

    @Override
    protected void onDestroy() {
        if (mBound) {
            unbindService(mConnection);
            stopService(new Intent(getApplicationContext(), MedikationsPlanService.class));
            mBound = false;
            Log.d("MedikationsPlan", "MedikationsActivity: Unbind mConnection");
        }
        if (mMBound) {
            unbindService(messengerConnection);
            stopService(new Intent(getApplicationContext(), MedikationsPlanHandler.class));
            mMBound = false;
            Log.d("MedikationsPlan", "MedikationsActivity: Unbind mMessengerConnection");
        }
        super.onDestroy();
    }

    @Override
    public void onListFragmentInteraction(Medikationsplan item, int position) {
        Log.d("MedikationsPlan", "Item: " + item.getInstanzId() + " selected on Position: " + position);

        Fragment blockFragment = new BlockFragment();
        blockFragment = BlockFragment.newInstance(position);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.remove(blockFragment);
        transaction.replace(R.id.fragmentcontainer, blockFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    /*
     * --------------------------------------------------------------------------------------------
     * --------------------- WHAT HAPPENS WHEN A BUTTON IS CLICKED? -------------------------------
     */

    @Override
    public void onListFragmentLongClick(Medikationsplan item, int position) {
        mph.deleteData(position);
    }

    /**
     * Scannt einen QR Code ein. Dabei wird ein INTENT von ZXING verwendet und eine APP
     * mit "startActivityForResult" aufgerufen.
     */
    public void openZxingQRScanner() {
        try {
            //start the scanning activity from the com.google.zxing.client.android.SCAN intent
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
            //on catch, show the download dialog
            showDialog(MedikationsplanActivity.this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
        }
    }

    /**
     * Gehört zu scanQR Methode. Nach erfolgreichem scannen, wird von der APP eine Antwort als String
     * zurückgegeben. Dieser String wird zum MedikationsPlanService weitergegeben um einen Medikations-
     * plan aus dem XML-String zu erzeugen.
     *
     * @param requestCode
     * @param resultCode
     * @param intent      von der ZXING-Applikation
     */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                //get the extras that are returned from the intent
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                mph.createMedikationsPlan(contents);
                Log.d("MedikationsPlan", "createMedikationsPlan");
            }
        }
    }

    /**
     * Öffnet ein Dialog, wo der Benutzer einen XML Text einfügen kann, welche dann in einen Medikationsplan
     * geparsed wird
     *
     * @param v
     */
    public void openXmlInsertDialog(View v) {
        final Dialog xmliDialog = new Dialog(MedikationsplanActivity.this);
        xmliDialog.setContentView(R.layout.xml_insert_dialog);
        final EditText etText = (EditText) xmliDialog.findViewById(R.id.xmli_etText);
        Button btSend = (Button) xmliDialog.findViewById(R.id.xmli_btEnter);
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = etText.getText().toString();
                mph.createMedikationsPlan(str);
                xmliDialog.dismiss();
            }
        });
        xmliDialog.show();
    }

    /*
     * --------------------------------------------------------------------------------------------
     * ----------------------------------- OTHER STUFFS -------------------------------------------
     */
    public void refreshView() {
        if (mph != null)
            if (mph.getMedikationsplanList() != null)
                mpl = mph.getMedikationsplanList();
        ((MedikationsplanFragment) mpFragment).updateView(mpl);
    }

    public List<Medikationsplan> getMedikationplan() {
        return mpl;
    }

    public class ReceivedNotificationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MedikationsPlanHandler.MSG_MP_UPDATED:
                    refreshView();
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

}
