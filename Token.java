package de.allianz.cloudmq.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.allianz.apigateway.tokenvalidation.http.HttpClient;
import de.allianz.apigateway.tokenvalidation.http.HttpResponse;
import de.allianz.apigateway.tokenvalidation.http.JDKHttpClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Token {
    public static final int MAX_TRIES = 10;
    public static final Logger LOG = LogManager.getLogger(Token.class.getName());

    private String url;

    private String tokenFull;
    private HashMap<String, Object> tokenMap;
    final Map<String, Object> params;
    private JDKHttpClient httpClient;

    private HostnameVerifier allHostsValid = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };

    public Token() {
        this(null);
    }

    public Token(String providerURL) {
        this(providerURL, "", "");
    }

    public Token(String providerURL, final String gduid, final String rqi) {
        httpClient = new JDKHttpClient();
        setDev();
        if (providerURL != null && !providerURL.isEmpty()) url = providerURL;
        tokenFull = null;
        tokenMap = null;
        params = new HashMap<>();
        params.put("grant_type", "client_credentials");
        params.put("gduid", gduid);
        params.put("rqi", rqi);
    }

    public String getToken() {
        // Token already exists, checking for expiration!
        if (tokenMap != null) {
            return isValid() ? (String) tokenMap.get("access_token") : getToken();
        } else {
            HttpResponse response = null;
            int i = 0;
            do {
                try {
                    if (i != 0) {
                        wait(1000);
                    }
                    response = httpClient.httpPost(url, params);
                    if (response.statusCode != 200) {
                        tokenFull = new String(response.content);
                        LOG.info("HTTP Response: " + response.content.toString());

                        // MAP response to a Map
                        if (tokenFull != null && !tokenFull.isEmpty()) {
                            tokenMap = new ObjectMapper().readValue(tokenFull, HashMap.class);
                            LOG.info("Token Mapping Class: " + tokenMap.toString());
                        }
                    } else {
                        LOG.error("Error post message to " + url + ". " + tokenFull);
                    }
                } catch (final IOException e1) {
                    i++;
                    tokenMap = null;
                    LOG.error("Error post message to " + url);
                    e1.printStackTrace();
                } catch (final InterruptedException e) {
                    LOG.error("Interrupted!");
                    e.printStackTrace();
                }
            } while (response == null & i < MAX_TRIES);
        }
        return (String) tokenMap.get("access_token");
    }

    public boolean isValid() {
        if (tokenMap != null) {
            final int currentTime = (int) (System.currentTimeMillis() / 1000L);
            final int exp = (int) tokenMap.get("exp");
            // final int expIn = (int) tokenMap.get("expires_in");
            if (currentTime + 60 < exp) {
                return true;
            }
            tokenMap = null;
        }
        return false;
    }

    public String getTokenFull() {
        if (tokenMap == null) getToken();
        return tokenFull;
    }

    public void setDev() {
        httpClient.setHostnameVerifier(allHostsValid);
        url = "https://test-oauth-provider.xxxxx/oauth2xxx/oauth/token";

    }

    public void setSi() {
        url = "https://si-oauth-provider.xxxxx/oauth2xxx/oauth/token";
    }

    public void setProd() {
        url = "https://oauth-provider.xxxxx/oauth2xxx/oauth/token";
    }

    public String getDummyToken() {
        final HttpClient httpClient = new JDKHttpClient();
        HttpResponse response;
        try {
            response = httpClient.httpGet("http://localhost:8080/api/token/generate?rqi=" + params.get("rqi") + "&subKey=%22misc:anon%22&subValue=%22%22");
            LOG.info("Body: " + response.toString());
            final String token = "{\"token\":\"" + new String(response.content) + "\"}";
            LOG.info("Using Token: " + token);
            return token;
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
